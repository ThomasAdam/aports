# Contributor: Robin Candau <robincandau@protonmail.com>
# Maintainer: Robin Candau <robincandau@protonmail.com>
pkgname=nwg-panel
pkgver=0.9.60
pkgrel=0
pkgdesc="GTK3-based panel for sway and Hyprland Wayland compositors"
url="https://github.com/nwg-piotr/nwg-panel"
arch="noarch"
license="MIT"
depends="python3 py3-gobject3 py3-i3ipc py3-cairo py3-dasbus py3-netifaces py3-psutil py3-requests bluez-btmgmt brightnessctl swaybg"
makedepends="py3-gpep517 py3-build py3-installer py3-wheel py3-setuptools gtk+3.0-dev libayatana-appindicator-dev gtk-layer-shell-dev glib-dev playerctl-dev"
subpackages="$pkgname-doc $pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/nwg-piotr/nwg-panel/archive/$pkgver.tar.gz"
options="!check" # no test suite

build() {
	gpep517 build-wheel --wheel-dir .dist --output-fd 3 3>&1 >&2
}

package() {
	gpep517 install-wheel --destdir "$pkgdir" .dist/*.whl

	install -Dm 644 "$pkgname.svg" "$pkgdir/usr/share/pixmaps/$pkgname.svg"
	install -Dm 644 nwg-shell.svg "$pkgdir/usr/share/pixmaps/nwg-shell.svg"
	install -Dm 644 nwg-processes.svg "$pkgdir/usr/share/pixmaps/nwg-processes.svg"
	install -Dm 644 "$pkgname-config.desktop" "$pkgdir/usr/share/applications/$pkgname-config.desktop"
	install -Dm 644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
	install -Dm 644 README.md "$pkgdir/usr/share/doc/$pkgname/README.md"
}

sha512sums="
76a47a3d1f067ef439dee935d1e4cba9c6021cdaf3c7e344fdb5eece02778308ce0ec67a3cd12b96fd3134747d1d9f7f3a9797d3f1b831d13fff22fd5c138934  nwg-panel-0.9.60.tar.gz
"
